using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Block : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] public int weightMin;
    [SerializeField] public int weightMax;
    [SerializeField] public float height;
    [SerializeField] public bool outOfCamera;
    private int maxWeight;
    
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RandomSize()
    {
        maxWeight = Random.Range(weightMin, weightMax);
        transform.localScale = new Vector3(maxWeight,height,0);
    }
    
    public void RandomSize(int rangemin, int rangemax)
    {
        maxWeight = Random.Range(rangemin, rangemax);
        
        transform.localScale = new Vector3(maxWeight,height,0);
    }

    public void RandomPosition()
    {
        float temp = maxWeight;
        float x = GameController.instance.previousBlock.transform.position.x;
        float y = GameController.instance.previousBlock.transform.position.y;
        float weight = x + 2 + (temp / 2);
        transform.position = new Vector3(weight, y + 2, 0);
        GameController.instance.previousBlock = this;
    }

    public void RandomPosition(float x, float y)
    {
        float temp = maxWeight;
        float weight = x + 3 + temp;
        transform.position = new Vector3(x, y, 0);
       
    }
    
    public void SetPosition(float x, float y)
    {
        transform.position = new Vector3(x, y, 0);
    }
    
    
}

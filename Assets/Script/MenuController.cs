using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class MenuController : MonoBehaviour
{
    // Start is called before the first frame update
    public SceneManager SceneManager;
    [SerializeField] public Canvas Replay;
    [SerializeField] public Canvas GameOver;


    void Start()
    { 
        
    }

    // Update is called once per frame
    void Update()
    {
        //GetStatusMenu();
    }

    public void GetStatusMenu()
    {
        if (GameController.instance.gameStatus.Contains("lose"))
        {
            Replay.gameObject.SetActive(true);
            GameController.instance.status.text = "Game Over";
        }
        if (GameController.instance.gameStatus.Contains("GameOver") || GameController.instance.gameStatus.Contains("GameWin"))
        {
            GameOver.gameObject.SetActive(true);
            Replay.gameObject.SetActive(false);
            GameController.instance.status.text = GameController.instance.gameStatus;
            Debug.Log(GameController.instance.gameStatus);
            
        }
    }

    public void Replays()
    {
        Time.timeScale = 1;
        GameController.instance.gameStatus = "Replay";
        Vector2 resp = GameController.instance.respawnPoint;
        resp.y += 1f;
        GameController.instance.Player.transform.position = resp;
        
        GameController.instance.Player.Heartbar[GameController.instance.Player.Heart - 1].SetActive(false);
        GameController.instance.Player.Heart -= 1;
        //GameController.instance.Point -= 1;
        Replay.gameObject.SetActive(false);
    }

    public void Restart()
    {
        if (GameController.instance.gameStatus.Contains("GameWin"))
        {
            GameController.instance.dataPoint._highScore.Score = GameController.instance.maxRangePoint ;
            GameController.instance.dataPoint.SaveJsonData();
        }
   
        SceneManager.LoadScene("Game");
        
    }
    
    public void StartGame()
    {
        GameController.instance.isPlayerControl = true;
        Time.timeScale = 1;
    }
    

}
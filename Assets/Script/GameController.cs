using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    // Start is called before the first frame update
    public static GameController instance;
    public SceneManager SceneManager;
    public int Point;
    public DataPoint dataPoint;
    public Block previousBlock;
    public GameObject StadingBlock;
    public Block nextBlock;
    [SerializeField] public int maxRangePoint;
    public string gameStatus;
    public bool isPlayerControl = false;
    [SerializeField] public Text PointText;
    public MenuController MenuController;
    public Text status;
    public Vector2 respawnPoint;
    public Player Player;
    
    string saveFile;
    private void Awake()
    {
        if (instance == null)
            instance = this;
        maxRangePoint = dataPoint.Score;

    }

    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        //SetGameStatusLogic();
    }

    public void SetPoint()
    {
        PointText.text = Point + "/" + maxRangePoint;
    }

    public void SetGameStatusLogic()
    {
        if (Point == maxRangePoint)
        {
            maxRangePoint += 1;
            gameStatus = "GameWin";
            Time.timeScale = 0;
        }
        MenuController.GetStatusMenu();
        
    }

    
    
}

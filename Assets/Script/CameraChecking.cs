using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class CameraChecking : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Camera mainCamera;
    [SerializeField] public RandomBlock RandomBlock;
    private CinemachineVirtualCamera _virtualCamera;
    void Start()
    {
        mainCamera = gameObject.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckCar();
    }

    private void CheckCar()
    {
        var lineOutOfViewPosition = RandomBlock.BlockList.Peek().transform.position;
        var viewPos = mainCamera.WorldToViewportPoint(lineOutOfViewPosition);

        if (viewPos.x > 1 || viewPos.x < 0 ||  viewPos.y < 0 || viewPos.y > 1)
        {
            RandomBlock.BlockList.Peek().outOfCamera = true;
        }
        else
        {
            RandomBlock.BlockList.Peek().outOfCamera = false;
        }
        
        
    }
    
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomBlock : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] public Block rootBlock;
    public Queue<Block> BlockList;
   // public Block previousBlock;
    public int Lenght;
    
    
    void Start()
    {
        BlockList = new Queue<Block>();
        CreatBlockList();
    }

    // Update is called once per frame
    void Update()
    {
        SetNextBlock();
    }

    public void SetNextBlock()
    {
        if (BlockList.Peek().outOfCamera)
        {
            GameController.instance.nextBlock = BlockList.Peek();
            Block nextBlock = BlockList.Dequeue();
            nextBlock.RandomSize();
            nextBlock.RandomPosition();
            BlockList.Enqueue(nextBlock);
        }
        
        
    }

    public void CreatBlockList()
    {
        BlockList.Enqueue(rootBlock);
        for (int i = 1; i <= Lenght; i++)
        {
            Block newBlock = Instantiate(rootBlock);
            newBlock.RandomSize(1,3);
            newBlock.RandomPosition(rootBlock.transform.position.x + (i * 2), rootBlock.transform.position.y + (i * 2));
            BlockList.Enqueue(newBlock);
            if (i == Lenght)
                GameController.instance.previousBlock = newBlock;
        }
    }
    
    
}

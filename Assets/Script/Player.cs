using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] public float Speed;
    [SerializeField] public float HeightForce;
    public Rigidbody2D Rigidbody2D;
    public bool isGround = true;
    [SerializeField]
    public GameObject[] Heartbar;
    public int Heart;
    public GameObject ground;
    void Start()
    {
        Rigidbody2D = GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.instance.isPlayerControl)
        {
            if(GameController.instance.gameStatus != "Replay")
                AutoMove();
            Jumps();
            CheckFall();
        }
        
    }

    private void FixedUpdate()
    {
        
    }

    public void AutoMove()
    {
        Vector3 Movement =  Vector3.right * (Speed * Time.deltaTime);
        transform.Translate(Movement);
    }

    public void Jumps()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isGround)
        {
            Vector2 force = Vector2.up * HeightForce;
            Rigidbody2D.AddForce(force, ForceMode2D.Impulse);
            isGround = false;
            GameController.instance.gameStatus = "ingame";
            Debug.Log(GameController.instance.gameStatus );
        }
            
    }
    

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            //Debug.LogError(transform.position + " - " + other.gameObject.transform.position);
            Vector2 player = transform.position;
            Vector2 block = other.gameObject.transform.position;
            
            Vector3 hit = other.contacts[0].normal;

            float onTop = Vector3.Angle(hit, Vector3.forward);
            float left = Vector3.Angle(hit, Vector3.up);
            if (Mathf.Approximately(onTop,90))
            {
                if (Mathf.Approximately(left, 0))
                {
                    isGround = true;
                    if (other.gameObject == GameController.instance.StadingBlock)
                    {
                        return;
                    }
                
                    GameController.instance.Point += 1;
                    GameController.instance.StadingBlock = other.gameObject;
                    GameController.instance.respawnPoint = GameController.instance.StadingBlock.transform.position;
                    GameController.instance.maxRangePoint = GameController.instance.dataPoint._highScore.Score;
                    GameController.instance.SetGameStatusLogic();
                    GameController.instance.dataPoint.ResetScore();
                }
            }
            else
            {
                GameController.instance.gameStatus = "lose";
                Time.timeScale = 0f;
            }
        }
        
    }

    public void CheckFall()
    {
        if (GameController.instance.gameStatus.Contains("ingame") && Mathf.Abs(transform.position.y) - Mathf.Abs(GameController.instance.StadingBlock.transform.position.y) < 0)
        {
            if (Heart - 1 == 0)
            {
                GameController.instance.gameStatus = "GameOver";
                GameController.instance.SetGameStatusLogic();
                Time.timeScale = 0f;
                return;
            }
            if (GameController.instance.StadingBlock == ground)
            {
                return;
            }
            GameController.instance.gameStatus = "lose";
            GameController.instance.SetGameStatusLogic();
            Time.timeScale = 0f;
        }
        
    }
    
    
}

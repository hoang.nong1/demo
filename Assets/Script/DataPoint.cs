using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class DataPoint : MonoBehaviour
{
    [SerializeField] public Text ScoreBox;
    public int Score = 0;
    public TextAsset DataJson;
    string saveFile;
    [Serializable]
    public class HighScore
    {
        public int Score;
        public HighScore(int score)
        {
            Score = score;
        }
    }
    public HighScore _highScore;

    public void Awake()
    {
        saveFile = DataJson.name + ".json";
        LoadJsonData();
    }

    void Start()
    {
       ResetScore();
    }

    // Update is called once per frame
    void Update()
    {
        //ScoreBox.text = GameController.instance.Point + "/" + _highScore.Score;
    }

    public void ResetScore()
    {
        ScoreBox.text = GameController.instance.Point + "/" + _highScore.Score;
    }

    public void LoadJsonData()
    {
        string json = ReadDataJson(saveFile);
        JsonUtility.FromJsonOverwrite(json, _highScore);
        GameController.instance.maxRangePoint = _highScore.Score;
    }

    public void SaveJsonData()
    {
        string jsonString = JsonUtility.ToJson(_highScore);
        WriteDataJson(saveFile, jsonString);
        Debug.LogError(jsonString);
    }
    
    

    public void WriteDataJson(string fileName, string json)
    {
        string path = GetFilePath(fileName);
        FileStream fileStream = new FileStream(path, FileMode.Create);
        using (StreamWriter writer = new StreamWriter(fileStream))
        {
            writer.Write(json);
        }
    }
    
    public string ReadDataJson(string fileName)
    {
        string path = GetFilePath(fileName);
        using (StreamReader reader = new StreamReader(path))
        {
            string json = reader.ReadToEnd();
            return json;
        }
    }
    
    private string GetFilePath(string saveFile)
    {
#if UNITY_EDITOR
        Debug.Log(Application.dataPath);
        return Application.dataPath + "/" + saveFile;
        
#endif
        return Application.persistentDataPath + "/" + saveFile;
    }
    
    
}
